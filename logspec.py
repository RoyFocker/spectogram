import matplotlib.pyplot as plt
from scipy import signal
from scipy.io import wavfile
import numpy as np
from sklearn import preprocessing #Normalizacion en el espectograma

sample_rate, samples = wavfile.read('samples/LIFECLEF2017_BIRD_XC_WAV_RN34241.wav')
frequencies, times, spectrogram = signal.spectrogram(samples, sample_rate)

plt.pcolormesh(times, frequencies, np.log(spectrogram))
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.show()