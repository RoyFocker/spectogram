from scipy.io import wavfile #Leer wavs
import numpy as np #Manejar datos como arreglos para graficar
import matplotlib.pyplot as plt #Para graficar

AudioName = "/home/royfocker/SS/espectograma/samples/LIFECLEF2017_BIRD_XC_WAV_RN34241.wav" #Lectura de wav
fs, Audiodata = wavfile.read(AudioName) #Obtener datos numericos del archivo de audio leido
plt.plot(Audiodata) #Graficar
plt.title('Audio en el tiempo',size=16) #Etiqueta
plt.show() #Mostrar grafica