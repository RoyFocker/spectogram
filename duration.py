import wave
import contextlib
fname = 'samples/LIFECLEF2017_BIRD_XC_WAV_RN34203.wav'
with contextlib.closing(wave.open(fname,'r')) as f:
    frames = f.getnframes()
    rate = f.getframerate()
    duration = frames / float(rate)
    print(duration)