#LAHA

#Importaciones
import matplotlib.pyplot as plt #Permite graficar
from essentia.standard import * #Loader para wavs
import numpy as np #Operaciones matematicas para el espectograma
from sklearn.preprocessing import normalize #Normalizacion en el espectograma
import parselmouth #Trabajar con batches de archivos
import glob #Lectura de wavs
import os.path #Lectura de wavs
import pickle #Guardar en disco espectogramas
import statistics #Calcular media para matrices de recorte
import wave #Calcular duraciones wavs
import contextlib #Abrir wavs y obtener frames
from skimage import morphology #Erosion and dilation

import time

#Obtener espectograma
def spec(audio,framesize=1024,hopsize=256,alpha=0.8,cutoff=256,norm=True,pre=True):
    w = Windowing(type = 'hann') #Ventaneo de tipo Hann
    spectrum = Spectrum() #Cambio al dominio de la frecuencia
    audio=np.array(audio) #Transformar el audio a un arreglo
    if pre:
        audio[1:]=audio[1:]-(alpha*audio[:-1])
    nbins=int(framesize/2+1) #nbins para el muestreo
    spectogram=np.zeros((int((len(audio)-framesize)/hopsize)+1,nbins))#Se crea el arreglo que compondra el espectograma
    prior=0.0
    for ix,fstart in enumerate(range(0, len(audio)-framesize, hopsize)):#Saltos con ventaneo
        frame = audio[fstart:fstart+framesize] #Se obtiene el frame a analizar
        spectogram[ix,:]=spectrum(w(frame)) #Se convierte al dominio de la frecuencia
    #spectogram=np.log(spectogram+1e-12) #Aplicacion de funcion logaritmica al espectograma
    if norm:
        spectogram=normalize(spectogram) #Normalizacion del espectograma
        pass
    if cutoff: #Ajuste de espectograma
        spectogram=spectogram[:,:cutoff]
    return np.transpose(np.fliplr(spectogram)) #Regresa el espectograma obtenido

#Leer wavs en directorio y agregarlos a lista
def reader(route,lim,start=0): #route es la ruta de donde se leeran los wavs, limite el numero de wavs a leer y start es una variable auxiliar por si se desea comenzar a leer a partir de un indice distinto a 0
    out=[] #Lista que contendra los espectogramas de salida
    times=[] #Lista que contendra los tiempos de los wav leidos
    for wave_file in glob.glob(route+"/*.wav"): #Iteracion en el directorio especificado con todos los archivos tipo wav
        if start==lim: #Condicion que rompe la lectura de wavs cuando se ha leido el numero de wavs especificado
            break
        else:
            start+=1 #Variable de control
        print("Processing {}...".format(wave_file)) #Impresion de los archivos obtenidos
        with contextlib.closing(wave.open(wave_file,'r')) as f: #Obtener duracion del wav
            frames = f.getnframes()
            rate = f.getframerate()
            duration = frames / float(rate)
            times.append(duration) #Agregar duracion a lista de duraciones
        s=parselmouth.Sound(wave_file) #Cargando en esta variable se podra acceder a un modulo de pre-enfasis
        s.pre_emphasize() #Tecnica de reduccion de ruido, las seniales mas debiles son acentuadas
        loader=essentia.standard.MonoLoader(filename=wave_file) #Se carga el archivo de audio leido
        audio=loader() #Se carga el audio con el loader
        audiospec=spec(audio)#Obtencion del espectograma del audio
        out.append(audiospec) #Se agrega el espectograma a la lista que los contiene
    return out,times #La salida es una lista con espectogramas

#Muestra graficamente los espectogramas de los wavs
def plotter(spectograms,times,specific=False,i=0): #spectograms es una lista que contenga espectogramas para iterar, times contiene las duraciones de los wavs, si specific es True entonces se grafica especificamente un espectograma que por default es el primero de la lista
    spectogramsbw=blackandwhite(spectograms)
    #spectogramsbw=adjustment(spectogramsbw)
    if specific==True: #Si se paso a True como un parametro entonces solo se grafica un espectograma en especifico
        time=np.linspace(0,times[i],len(spectograms[i][0])) #start,end,step para tiempo
        freq=np.linspace(0,5000,len(spectograms[i])) #start,end,step para frecuencia
        plt.pcolormesh(time,freq,spectograms[i]) #Muestra el elemento especificado
        plt.ylabel('Frequency [Hz]') #Etiqueta en el eje Y
        plt.xlabel('Time [s]') #Etiqueta en el eje X
        plt.show() #Muestra la grafica con los elementos dados arriba
        return
    for x in range(0,len(spectograms)): #Itera en la lista de espectogramas
        time=np.linspace(0,times[x],len(spectograms[x][0])) #start,end,step para tiempo
        freq=np.linspace(0,5000,len(spectograms[x])) #start,end,step para frecuencia

        fig,axs=plt.subplots(2)

        c=axs[0].pcolormesh(time,freq,spectograms[x])
        c=axs[1].pcolormesh(time,freq,spectogramsbw[x],cmap='Greys') #Muestra elemento por elemento

        plt.ylabel('Frequency [Hz]') #Etiqueta en el eje Y
        plt.xlabel('Time [s]') #Etiqueta en el eje X
        fig.tight_layout()
        plt.show() #Muestra la grafica con los elementos dados arriba
    return

#Erosion y dilatacion de los pixeles seleccionados
def adjustment(spectograms):
    for x in range(0,len(spectograms)):
        spectograms[x]=morphology.erosion(spectograms[x])
        spectograms[x]=morphology.dilation(spectograms[x])
    return spectograms

#Seleccionar pixeles
def blackandwhite(spectrograms):
    bwspecs=[]
    bw=[[]]
    for s in range(0,len(spectrograms)):
        spectrograms[s]=np.abs(spectrograms[s])
        X=np.max(spectrograms[s],axis=1)*0.6
        Y=np.max(spectrograms[s],axis=0)*0.6
        bw=np.zeros((spectrograms[s]).shape)
        
        #spectograms[s][spectograms[s]>0.6]=1

        for x in range(0,len(X)):
            for y in range(0,len(Y)):
                if spectrograms[s][x][y] > (X[x]*0.6) and spectrograms[s][x][y] > (Y[y]*0.6):
                    bw[x][y]=1
                else:
                    bw[x][y]=0
        bwspecs.append(bw)
    return bwspecs

#######################################################Main#######################################################
specs,times=reader("/home/royfocker/SS/espectograma/samples",1) #Obtiene una lista con un numero especificado de espectogramas del directorio especificado 
#pickle.dump(specs,open("specs.pickle","wb")) #Exportacion a un archivo con extension pickle
#prueba=pickle.load(open("specs.pickle", "rb")) #Lectura de un archivo con extension pickle
plotter(specs,times)
######################################################Pruebas######################################################
#reader (+spec) 2.91502714157
#spec 0.20429110527
#bw 15.6647891998
#plotter 42.7258608341
#Total: +-59.3390519619