from pydub import AudioSegment
import glob 

def reader(route,lim,start=0):
    out=[] 
    for wave_file in glob.glob(route+"/*.wav"):
        if start==lim: 
            break
        else:
            start+=1
        print("Processing {}...".format(wave_file))
        out.append(wave_file)
    return out

def converter(names):
	for audio in names:
		sound = AudioSegment.from_wav(audio)
		sound = sound.set_channels(1)
		sound.export(audio, format="wav")

#Main
specs_names=reader("/home/royfocker/SS/espectograma/samples",50)
converter(specs_names)